#!/usr/bin/env python3

#
# ds8k_perfs.py - stat-like utility to display Open-system performance metrics for IBM DS8000 storage system
#

# ----------------------------------------------------------------------------------------------------------------------
# CHANGELOG
# ----------------------------------------------------------------------------------------------------------------------
# 2018.09.19    v0.1    Mikhail Zakharov <zmey20000@yahoo.com>
#               Project starts
# 2018.09.25    v0.2    Mikhail Zakharov <zmey20000@yahoo.com>
#               IBMTSDS_VolumeStatistics instance scanning/parsing
# 2018.10.17    v0.3    Mikhail Zakharov <zmey20000@yahoo.com>
#               The first full collecting cycle
# 2018.11.07    v0.4    Mikhail Zakharov <zmey20000@yahoo.com>
#               Command-line options, interactive password, selected ID's list
# 2018.11.12    v0.5    Mikhail Zakharov <zmey20000@yahoo.com>
#               Usage, help, readme and setup
# 2018.11.13    v1.0    Mikhail Zakharov <zmey20000@yahoo.com>
#               The first public release
#
# ----------------------------------------------------------------------------------------------------------------------
# SOFTWARE REQUIREMENTS
# ----------------------------------------------------------------------------------------------------------------------
# 1. Python3
# 2. pywbem module
#
# ----------------------------------------------------------------------------------------------------------------------
# STORAGE SYSTEM CONFIGURATION
# ----------------------------------------------------------------------------------------------------------------------
# Enable SMI-S protocol on the IBM DS8000 storage system:
# 1. Using DSCLI utility connect with the storage system HMC
# 2. Run: manageaccess -ctrl cim -action enable -level 800131a
#
# ----------------------------------------------------------------------------------------------------------------------
# USAGE
# ----------------------------------------------------------------------------------------------------------------------
# ds8k_perfs.py - stat-like utility to display Open-system performance metrics for IBM DS8k storage system
#
# Volumes:
#         ds8k_perfs.py -v -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]
# FC-Interfaces:
#         ds8k_perfs.py -i -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]
# Ranks:
#         ds8k_perfs.py -r -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]
#
# Common options:
#         -a address -u user [-p password]
#                 Host address and user credentials. If password is omitted, it will be requested interactively.
#
#         [-l list]
#                 Limits IDs to fetch statistics for in format of "1,2a,3,c55,d1000".
#
#         [-f interval]
#                 Samples interval. Default minimum is 5 minutes.
#                 It is not recommended to set values less then the default one.
#
#         [-s separator]
#                 Use separator for the output. Default is to print data in stat-like columns.
#
#         [-z]
#                 Show lines with all zeroes in counters. Default is to skip them.
#
#         [-A]
#                 Show absolute performance counters on the first iteration. Default is to skip the first output.
#
#         [-d]
#                 Show Date and Time as the first column of the output. Default is to hide it.
#
#         [-h]
#                 Display this basic usage along with information about performance metric acronyms
#

import sys
import pywbem
import time
import getopt
import getpass


target_port = '6989'                                # Default port for SMI-S (HTTPS)
target_proto = 'https'                              # Default protocol
target_namespace = 'root/ibm'

query_language = 'WQL'                              # IBM DS8K doesn't understand DMTF:CQL
delimiter = ''                                      # Output fields delimiter or leave it empty to run in "stats" mode
s_interval = 5 * 60                                 # Statistics samples interval in seconds
skip_zeroes = True                                  # Don't show empty lines
skip_first = True                                   # Skip the first output with historical "totals" or not
skip_date = True                                    # Whether to skip printing a column with current date/time or not

p_class = 'IBMTSDS_VolumeStatistics'                # By default show lite version of performance statistic for volumes

nop_error = 'Error! You must specify all mandatory options to get data.'

s_classes = {
    'IBMTSDS_VolumeStatistics': {
        # Request metrics header
        'r_metrics': [
            'AbbreviatedID', 'ReadIOs', 'WriteIOs', 'BytesRead', 'BytesWritten', 'PhysicalStorageReadOperations',
            'PhysicalStorageWriteOperations', 'PhysicalStorageBytesRead', 'PhysicalStorageBytesWritten',
            'PhysicalStorageReadTime', 'PhysicalStorageWriteTime', 'AccumulatedReadTime', 'AccumulatedWriteTime'
        ],
        # Display metrics header
        'd_metrics': [
            'ID', 'rIO/s', 'wIO/s', 'rKB/s', 'wKB/s', 'rphIO/s', 'wphIO/s', 'rphKB/s', 'wphKB/s', 'rt_ph', 'wt_ph',
            'rt', 'wt'
        ]
    },
    'IBMTSDS_FCPortStatistics': {
        'r_metrics': [
            'AbbreviatedID', 'TotalIOs', 'SCSIReadOperations', 'SCSIWriteOperations',
            'PPRCReceivedOperations', 'PPRCSendOperations', 'ECKDReadOperations', 'ECKDWriteOperations',
            'SCSIBytesRead', 'SCSIBytesWritten', 'PPRCBytesReceived', 'PPRCBytesSend', 'ECKDBytesRead',
            'ECKDBytesWritten', 'ECKDAccumulatedReadTime', 'ECKDAccumulatedWriteTime', 'PPRCReceivedTimeAccumulated',
            'PPRCSendTimeAccumulated', 'SCSIReadTimeAccumulated', 'SCSIWriteTimeAccumulated'
        ],
        'd_metrics': [
            'ID', 'tIO/s', 'rSCSI_IO/s', 'wSCSI_IO/s', 'inPPRC_IO/s', 'outPPRC_IO/s', 'rECKD_IO/s', 'wECKD_IO/s',
            'rSCSI_KB/s', 'wSCSI_KB/s', 'inPPRCB_KB/s', 'outPPRC_KB/s', 'rECKD_KB/s', 'wECKD_KB/s', 'rtECKD', 'wtECKD',
            'intPPRC', 'outtPPRC', 'rtSCSI', 'wtSCSI'
        ]
    },
    'IBMTSDS_RankStatistics': {
        'r_metrics': [
            'AbbreviatedID', 'TotalIOs', 'ReadOperations', 'WriteOperations', 'BytesRead', 'BytesWritten',
            'ReadResponseTime', 'WriteResponseTime'
        ],
        'd_metrics': [
            'ID', 'IO/s', 'rIO/s', 'wIO/s', 'rKB/s', 'wKB/s', 'rt', 'wt'
        ]
    }
}


def usage(err_code=1, err_text='', hlp=False):
    if err_text:
        print(err_text, '\n', file=sys.stderr)

    print('ds8k_perfs.py - stat-like utility to display Open-system performance metrics for IBM DS8k storage system\n\n'
          'Volumes:\n'
          '\tds8k_perfs.py -v -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]\n'
          'FC-Interfaces:\n'
          '\tds8k_perfs.py -i -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]\n'
          'Ranks:\n'
          '\tds8k_perfs.py -r -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]\n'
          '\n'
          'Common options:\n'
          '\t-a address -u user [-p password]\n'
          '\t\tHost address and user credentials. If password is omitted, it will be requested interactively.\n\n'
          '\t[-l list]\n'
          '\t\tLimits IDs to fetch statistics for in format of "1,2a,3,c55,d1000".\n\n'
          '\t[-f interval]\n'
          '\t\tSamples interval. Default minimum is 5 minutes. '
          'It is not recommended to set values less then the default one.\n\n'
          '\t[-s separator]\n'
          '\t\tUse separator for the output. Default is to print data in stat-like columns.\n\n'
          '\t[-z]\n'
          '\t\tShow lines with all zeroes in counters. Default is to skip them.\n\n'
          '\t[-A]\n'
          '\t\tShow absolute performance counters on the first iteration. Default is to skip the first output.\n\n'
          '\t[-d]\n'
          '\t\tShow Date and Time as the first column of the output. Default is to hide it.\n\n'
          '\t[-h]\n'
          '\t\tDisplay this basic usage along with information about performance metric acronyms\n',
          file=sys.stderr)

    if hlp:
        print('\nPerformance statistics acronyms and display metrics', file=sys.stderr)
        for k in s_classes.keys():
            print('\n{}:\n{}'.format(k, 80 * '-'), file=sys.stderr)
            for mr, md in zip(s_classes[k]['r_metrics'], s_classes[k]['d_metrics']):
                print('{:<40s}{:<40s}'.format(md, mr), file=sys.stderr)

    sys.exit(err_code)


# -- MAIN --------------------------------------------------------------------------------------------------------------
target_addr = ''
target_user = ''
target_password = ''
Help = False

id_list = []

opts = ''
try:
    opts, args = getopt.getopt(sys.argv[1:], 'vira:u:p:l:f:s:zAdh')
except getopt.GetoptError as err:
    usage(1, str(err))

if not opts:
    usage(1, nop_error)

for opt, arg in opts:
    if opt == '-v':
        p_class = 'IBMTSDS_VolumeStatistics'
    elif opt == '-i':
        p_class = 'IBMTSDS_FCPortStatistics'
    elif opt == '-r':
        p_class = 'IBMTSDS_RankStatistics'
    elif opt == '-a':
        target_addr = arg
    elif opt == '-u':
        target_user = arg
    elif opt == '-p':
        target_password = arg
    elif opt == '-l':
        id_list = arg.split(',')
    elif opt == '-f':                                   # Statistics samples interval in seconds. Is set below.
        try:
            s_interval = int(arg) * 60
        except ValueError:
            usage(1, 'Error! Wrong -f value for sampling interval', Help)
    elif opt == '-s':
        delimiter = str(arg)
    elif opt == '-z':
        skip_zeroes = False
    elif opt == '-A':
        skip_first = False
    elif opt == '-d':
        skip_date = False
    elif opt == '-h':
        Help = True
    else:
        pass                                            # Unknown options are detected by getopt() exception above

if not target_addr or not target_user:
    usage(1, nop_error, Help)

if not target_password:
    target_password = getpass.getpass(prompt='{tu}@{ta} Password: '.format(ta=target_addr, tu=target_user))

target_url = target_proto + '://' + target_addr + ':' + target_port
wbemc = pywbem.WBEMConnection(target_url, (target_user, target_password), target_namespace, no_verification=True)
SI_new = {}

if delimiter:
    # Print the header once only
    print(delimiter.join('{}'.format(h) for h in s_classes[p_class]['d_metrics']))

aids = ''
if id_list:
    aids = ' or '.join(['AbbreviatedID="{}"'.format(aid) for aid in id_list])

while True:
    if aids:
        SI = wbemc.ExecQuery(query_language, 'SELECT {flds} from {pc} WHERE {ids}'.format(
            pc=p_class.split('-')[0], flds=','.join(s_classes[p_class]['r_metrics']), ids=aids))
    else:
        SI = wbemc.ExecQuery(query_language, 'SELECT {flds} from {pc}'.format(
             pc=p_class.split('-')[0], flds=','.join(s_classes[p_class]['r_metrics'])))

    SI_old = SI_new
    SI_new = {}                                                         # Cleanup old metrics

    for si in SI:
        aid = str(si['AbbreviatedID'])
        SI_new[aid] = {}
        for metric in s_classes[p_class]['r_metrics'][1:]:
            SI_new[aid][metric] = float(si[metric])

    if skip_first and not SI_old:
        # Skip printing the absolute historical totals on the first iteration
        time.sleep(s_interval)
        continue

    # Print the header on each iteration
    if not delimiter:
        if not skip_date:
            print('{dt:<20s}'.format(dt='Date/Time'), end='')                   # Date/Time
        print(''.join('{:<12s}'.format(h) for h in s_classes[p_class]['d_metrics']))

    deltas = []
    cdt = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())                  # Current date/time
    for aid in sorted(SI_new.keys()):
        try:
            # Count performance deltas for the numeric values (except the ID column)
            deltas = [(SI_new[aid][metric] - SI_old[aid][metric]) / s_interval
                      for metric in s_classes[p_class]['r_metrics'][1:]]
        except KeyError:
            # ...or use the fresh values if it's not possible to calculate deltas
            # this can happen on the first run for the historical totals or potentially
            # when a data-volume has just been added or removed
            deltas = [SI_new[aid][metric] for metric in s_classes[p_class]['r_metrics'][1:]]

        # Printout stats
        if not skip_zeroes or any(deltas):
            if not delimiter:                                                   # Align values to columns
                if not skip_date:
                    print('{dt:<20s}'.format(dt=cdt), end='')                   # Date/Time
                print('{id:>4s}'.format(id=aid), end='')                        # ID
                # Numerical statistic values
                print(''.join('{:>12.2f}'.format(d) for d in deltas), sep=delimiter)
            else:                                                               # Print delimiter-separated table
                if not skip_date:
                    print('{dt}{sep}'.format(dt=cdt, sep=delimiter), end='')    # Date/Time
                print('{id}{sep}'.format(id=aid, sep=delimiter), end='')        # ID
                # Numerical statistic values
                print(delimiter.join('{:.2f}'.format(d) for d in deltas), sep=delimiter)

    print(end='', flush=True)
    time.sleep(s_interval)
