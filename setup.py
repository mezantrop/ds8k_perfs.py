from setuptools import setup

setup(
    name='ds8k_perfs',
    version='1.0',
    py_modules=['ds8k_perfs'],
    install_requires=['pywbem'],
    url='https://gitlab.com/mezantrop/ds8k_perfs',
    license='',
    author='Mikhail Zakharov',
    author_email='zmey20000@yahoo.com',
    description='stat-like utility to display Open-system performance metrics for IBM DS8000 storage system'
)
