# ds8k_perfs.py

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

## ds8k_perfs.py - stat-like utility to display Open-system performance metrics for IBM DS8000 storage system

## Installation

Download the package, unzip if needed, then run:
```python3 setup.py install```

### Virtual environment installation
In order not to modify system python3 environment, you may consider virtual environment.
```
mkdir ds8k_perfs
cd ds8k_perfs
# Download the package, unzip if needed, copy all files into ds8k_perfs directory then:
python3 -m venv venv
source venv/bin/activate
(venv) python3 setup.py install
```

If you installed **ds8k_perfs.py** into a virtual environment, do not forget to activate it with "source /path/to/venv/bin/activate" command, for each session you are running **ds8k_perfs.py**.


* Requires Python 3 with 'pywbem' module
* Enable SMI-S protocol on the IBM DS8000 storage system:
    1. Using DSCLI utility connect with the storage system HMC
    2. Run: manageaccess -ctrl cim -action enable -level 800131a

```
ds8k_perfs.py - stat-like utility to display Open-system performance metrics for IBM DS8k storage system

Volumes:
        ds8k_perfs.py -v -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]
FC-Interfaces:
        ds8k_perfs.py -i -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]
Ranks:
        ds8k_perfs.py -r -a address -u user [-p password][-l list][-f interval][-s separator][-z][-A][-d]

Common options:
        -a address -u user [-p password]
                Host address and user credentials. If password is omitted, it will be requested interactively.

        [-l list]
                Limits IDs to fetch statistics for in format of "1,2a,3,c55,d1000".

        [-f interval]
                Samples interval. Default minimum is 5 minutes. It is not recommended to set values less then the default one.

        [-s separator]
                Use separator for the output. Default is to print data in stat-like columns.

        [-z]
                Show lines with all zeroes in counters. Default is to skip them.

        [-A]
                Show absolute performance counters on the first iteration. Default is to skip the first output.

        [-d]
                Show Date and Time as the first column of the output. Default is to hide it.

        [-h]
                Display this basic usage along with information about performance metrics acronyms

```
